﻿using LEF.Helpers;
using LEF.Models;
using LEF.SQL;
using System;

namespace LEF.CRUD
{
    /// <summary>
    /// Define el proceso para <strong>Eliminar</strong> un registro en una tabla
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DeleteAction<T> where T : DbModel
    {
        private readonly ModelContext<T> Context;

        /// <summary>
        /// Crea el entorno necesario para ejecutar acciónes <string>Delete</string>
        /// </summary>
        /// <param name="Context"></param>
        public DeleteAction(ModelContext<T> Context)
        {
            this.Context = Context;
        }

        /// <summary>
        /// Elimina un modelo de datos
        /// </summary>
        /// <param name="Condition">Condición <strong>Where</strong> (debes incluir la palabra "Where" en la condición)</param>
        /// <returns></returns>
        public Transaction<bool> Delete(string Condition)
        {
            try
            {
                if (Condition is null || Condition.Equals(string.Empty))
                    throw new Exception("No puedes eliminar registros sin una condición".ToUpper());

                if (!Condition.ToLower().Contains("where"))
                    throw new Exception("Debes incluir la palabra WHERE en la condición".ToUpper());

                var transaction = new DbRequest()
                {
                    Statement = $"DELETE FROM {DbModel.GetEntitylName<T>()} {Condition};"
                };

                return Context.DbContext.ExecBoolNonQuery(transaction);
            }
            catch (Exception ex)
            {
                return new Transaction<bool>()
                {
                    StatusCode = LEF_StatusCode.ERROR_CLASS.ToInt(),
                    Log = ex.Message.ToUpper()
                };
            }
        }
    }
}

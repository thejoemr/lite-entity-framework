﻿using LEF.Helpers;
using LEF.Models;
using LEF.SQL;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Xsl;

namespace LEF.CRUD
{
    /// <summary>
    /// Define el proceso para <strong>Insertar</strong> un nuevo registro en una tabla
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class InsertAction<T> where T :DbModel
    {
        private readonly ModelContext<T> Context;

        /// <summary>
        /// Crea el entorno necesario para ejecutar acciónes <string>INSERT</string>
        /// </summary>
        /// <param name="Context"></param>
        public InsertAction(ModelContext<T> Context)
        {
            this.Context = Context;
        }

        /// <summary>
        /// Inserta un modelo en la base de datos
        /// </summary>
        /// <param name="Preferences">Preferencias para la recuperación de datos</param>
        /// <param name="Models">Modelo de datos</param>
        /// <returns></returns>
        public virtual Transaction<bool> Insert(PropertyPreferences Preferences = null, params T[] Models)
        {
            try
            {
                if (Models is null || Models.Length == 0)
                    throw new Exception("Debes ingresar al menos un objeto".ToUpper());

                if (Models.ToList().Exists(x => x is null))
                    throw new Exception("No puedes ingresar objetos nulos".ToUpper());

                if (Preferences is null)
                    Preferences = Context.DefaultPreferences;

                var insertStatements = new List<string>();
                var insertParameters = new List<SqlParameter>();

                for (int i = 0; i < Models.Length; i++)
                {
                    var model = Models[i];
                    var info = DbModel.GetRequestInfo(model, Preferences, SqlPrefix: $"_{i}");

                    var insertStatement = info.GetInsertStatement();

                    insertStatements.Add(insertStatement);
                    insertParameters.AddRange(info.SqlParameters);
                }

                var transaction = new DbRequest()
                {
                    Statement = string.Join("\n", insertStatements),
                    Parameters = insertParameters
                };

                return Context.DbContext.ExecBoolNonQuery(transaction);
            }
            catch (Exception ex)
            {
                return new Transaction<bool>()
                {
                    StatusCode = LEF_StatusCode.ERROR_CLASS.ToInt(),
                    Log = ex.Message.ToUpper()
                };
            }
        }
    }
}

﻿using LEF.Helpers;
using LEF.Models;
using LEF.SQL;
using System;
using System.Collections.Generic;

namespace LEF.CRUD
{
    /// <summary>
    /// Define el proceso para <strong>Obtener</strong> información de una entidad de datos
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SelectAction<T> where T : DbModel
    {
        private readonly ModelContext<T> Context;

        /// <summary>
        /// Crea el entorno necesario para ejecutar acciónes <string>Select</string>
        /// </summary>
        /// <param name="Context"></param>
        public SelectAction(ModelContext<T> Context)
        {
            this.Context = Context;
        }

        /// <summary>
        /// Hace un <strong>SELECT</strong> de modelos de datos
        /// </summary>
        /// <param name="IncludeInheritance">Define si quieres utilizar las propiedades heredadas</param>
        /// <param name="Filter">Condición que deben cumplir los modelos</param>
        /// <returns>Model list</returns>
        public virtual Transaction<List<T>> Select(bool IncludeInheritance = false, string Filter = null)
        {
            try
            {
                var requestInfo = new DbRequestInfo<T>()
                {
                    PropertiesInfo = DbModel.GetProperties<T>(IncludeInheritance)
                };

                var selectStatement = requestInfo.GetSelectStatement();

                if(Filter != null && Filter != string.Empty)
                {
                    if (!Filter.ToLower().Contains("where"))
                        throw new Exception("Debes incluir la palabra WHERE en la condición".ToUpper());

                    selectStatement = $"{selectStatement} {Filter};";
                }

                var transaction = new DbRequest()
                {
                    Statement = selectStatement
                };

                var selectReq = Context.DbContext.ExecWithTable(transaction);

                if (!selectReq.IsSuccess())
                    return new Transaction<List<T>>()
                    {
                        Result = new List<T>(),
                        StatusCode = selectReq.StatusCode,
                        Log = selectReq.Log
                    };

                var output = new Transaction<List<T>>
                {
                    Result = selectReq.Result.ToList<T>()
                };

                return output;
            }
            catch (Exception ex)
            {
                return new Transaction<List<T>>()
                {
                    Result = new List<T>(),
                    StatusCode = LEF_StatusCode.ERROR_CLASS.ToInt(),
                    Log = ex.Message.ToUpper()
                };
            }
        }

        /// <summary>
        /// Hace un <strong>COUNT</strong> de modelos de datos
        /// </summary>
        /// <param name="Filter">Condición que deben cumplir los modelos</param>
        /// <returns>Model list</returns>
        public virtual Transaction<int> Count(string Filter = null)
        {
            try
            {
                var countStatement = string.Empty;

                if (Filter is null)
                    countStatement = $"select count(*) from {DbModel.GetEntitylName<T>()};";
                else
                    countStatement = $"select count(*) from {DbModel.GetEntitylName<T>()} {Filter};";

                var transaction = new DbRequest()
                {
                    Statement = countStatement
                };

                var countReq = Context.DbContext.ExecWithTable(transaction);

                var output = new Transaction<int>();

                if (countReq.IsSuccess())
                    output.Result = Convert.ToInt32(countReq.Result.Rows[0][0]);

                output.StatusCode = countReq.StatusCode;
                output.Log = countReq.Log;

                return output;
            }
            catch (Exception ex)
            {
                return new Transaction<int>()
                {
                    StatusCode = LEF_StatusCode.ERROR_CLASS.ToInt(),
                    Log = ex.Message.ToUpper()
                };
            }
        }
    }
}

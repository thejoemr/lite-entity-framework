﻿using LEF.Helpers;
using LEF.Models;
using LEF.SQL;
using System;
using System.Collections.Generic;
using System.Text;

namespace LEF.CRUD
{
    /// <summary>
    /// Define el proceso para <strong>Actualizar</strong> un registro en una tabla
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UpdateAction<T> where T : DbModel
    {
        private readonly ModelContext<T> Context;

        /// <summary>
        /// Crea el entorno necesario para ejecutar acciónes <string>UPDATE</string>
        /// </summary>
        /// <param name="Context"></param>
        public UpdateAction(ModelContext<T> Context)
        {
            this.Context = Context;
        }

        /// <summary>
        /// Actualiza un modelo de datos
        /// </summary>
        /// <param name="Preferences">Preferencias para la recuperación de datos</param>
        /// <param name="NewValues">Nuevos valores que tomarán los modelos de datos</param>
        /// <param name="Condition">Condición <strong>Where</strong> (debes incluir la palabra "Where" en la condición)</param>
        /// <returns></returns>

        public virtual Transaction<bool> Update(T NewValues, string Condition, PropertyPreferences Preferences = null)
        {
            try
            {
                if (NewValues is null)
                    throw new Exception("No puedes ingresar objetos nulos".ToUpper());

                if (Condition is null || Condition.Equals(string.Empty))
                    throw new Exception("No puedes actualizar registros sin una condición".ToUpper());

                if (!Condition.ToLower().Contains("where"))
                    throw new Exception("Debes incluir la palabra WHERE en la condición".ToUpper());


                if (Preferences is null)
                    Preferences = Context.DefaultPreferences;

                var info = DbModel.GetRequestInfo(NewValues, Preferences);

                var updateStatement = info.GetUpdateStatement();

                var transaction = new DbRequest()
                {
                    Statement = $"{updateStatement} {Condition}",
                    Parameters = info.SqlParameters
                };

                return Context.DbContext.ExecBoolNonQuery(transaction);
            }
            catch (Exception ex)
            {
                return new Transaction<bool>()
                {
                    StatusCode = LEF_StatusCode.ERROR_CLASS.ToInt(),
                    Log = ex.Message.ToUpper()
                };
            }
        }
    }
}

﻿using LEF.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace LEF.Helpers
{
    public static class Extensions
    {
        /// <summary>
        /// Verifica si un objeto es nulleable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsNullable<T>(this T obj)
        {
            if (obj is null)
                return true; // obvious

            Type type = typeof(T);

            if (!type.IsValueType)
                return true; // ref-type

            if (Nullable.GetUnderlyingType(type) != null)
                return true; // Nullable<T>

            return false; // value-type
        }

        /// <summary>
        /// Converts a DataTable object to an object list
        /// </summary>
        /// <typeparam name="T">Type of list</typeparam>
        /// <param name="DataSource">Data source</param>
        /// <returns>New object list</returns>
        public static List<T> ToList<T>(this DataTable DataSource) where T : class
        {
            try
            {
                if (DataSource is null || DataSource.Rows.Count == 0)
                    throw new Exception("La fuente de datos no puede ser nula".ToUpper());

                List<T> output = new List<T>();

                foreach (DataRow row in DataSource.Rows)
                {
                    T newObj = (T)Activator.CreateInstance(typeof(T));

                    foreach (DataColumn column in DataSource.Columns)
                    {
                        var propName = column.ColumnName;

                        if (typeof(T).GetProperty(propName) != null)
                        {
                            var value = row[propName];

                            if (value == DBNull.Value)
                                value = null;

                            typeof(T).GetProperty(propName).SetValue(newObj, value);
                        }
                    }

                    output.Add(newObj);
                }

                return output;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToUpper());
            }
        }

        /// <summary>
        /// Converts a DataTable object to an simple type list
        /// </summary>
        /// <typeparam name="T">Type of list</typeparam>
        /// <param name="DataSource">Data source</param>
        /// <param name="ColumnName">Nombre de la columna</param>
        /// <returns></returns>
        public static List<T> ToList<T>(this DataTable DataSource, string ColumnName)
        {
            try
            {
                if (DataSource is null || DataSource.Rows.Count == 0)
                    throw new Exception("La fuente de datos no puede ser nula".ToUpper());

                if (ColumnName.Equals(null) || ColumnName.Equals(string.Empty))
                    throw new Exception("Debes especificar el nombre de la columna que vas a unir".ToUpper());

                List<T> output = new List<T>();

                foreach (DataRow row in DataSource.Rows)
                    foreach (DataColumn column in DataSource.Columns)
                    {
                        var propName = column.ColumnName;

                        if (propName.Equals(ColumnName))
                        {
                            if (typeof(T).GetProperty(propName) != null)
                            {
                                var value = row[column.ColumnName];

                                if (value is null || value == DBNull.Value)
                                    output.Add(default);

                                output.Add((T)value);
                            }
                            else
                                throw new Exception($"No se encontró la propiedad {ColumnName}".ToUpper());
                        }
                    }

                return output;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToUpper());
            }
        }

        /// <summary>
        /// Crea una nueva lista a partir de la agrupación de valores de una propiedad
        /// </summary>
        /// <typeparam name="T">Type In</typeparam>
        /// <typeparam name="G">Type Out</typeparam>
        /// <param name="DataSource"></param>
        /// <param name="PropName"></param>
        /// <returns></returns>
        public static List<G> ListProperty<T, G>(this List<T> DataSource, string PropName)
        {
            try
            {
                List<G> output = new List<G>();

                if (DataSource is null || DataSource.Count == 0)
                    throw new Exception("La fuente de datos no puede ser nula".ToUpper());

                foreach (var obj in DataSource)
                {
                    var prop = typeof(T).GetProperty(PropName);

                    if (prop != null)
                    {
                        var value = prop.GetValue(obj);

                        if (value is null)
                            output.Add(default);
                        else
                            output.Add((G)value);
                    }
                }

                return output;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToUpper());
            }
        }

        /// <summary>
        /// Convierte una DataTable en un archivo Csv
        /// </summary>
        /// <param name="DataSource"></param>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public static bool WriteCsv(this DataTable DataSource, string FilePath)
        {
            try
            {
                StringBuilder data = new StringBuilder();

                //Taking the column names.
                for (int col = 0; col < DataSource.Columns.Count; col++)
                {
                    //Making sure that end of the line, shoould not have comma delimiter.
                    if (col == DataSource.Columns.Count - 1)
                        data.Append(DataSource.Columns[col].ColumnName.ToString().Replace(",", ";"));
                    else
                        data.Append(DataSource.Columns[col].ColumnName.ToString().Replace(",", ";") + ',');
                }

                data.Append(Environment.NewLine);//New line after appending columns.

                for (int iRow = 0; iRow < DataSource.Rows.Count; iRow++)
                {
                    for (int iCol = 0; iCol < DataSource.Columns.Count; iCol++)
                    {
                        ////Making sure that end of the line, shoould not have comma delimiter.
                        if (iCol == DataSource.Columns.Count - 1)
                            data.Append(DataSource.Rows[iRow][iCol].ToString().Replace(",", ";"));
                        else
                            data.Append(DataSource.Rows[iRow][iCol].ToString().Replace(",", ";") + ',');
                    }

                    //Making sure that end of the file, should not have a new line.
                    if (iRow != DataSource.Rows.Count - 1)
                        data.Append(Environment.NewLine);
                }

                using StreamWriter writer = new StreamWriter(FilePath);
                writer.WriteLine(data);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}

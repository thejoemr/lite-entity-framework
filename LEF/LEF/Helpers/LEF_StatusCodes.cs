﻿namespace LEF.Helpers
{
    /// <summary>
    /// Define los errores que pueden surgir al momento de realizar una transacción u operación con la libreria
    /// </summary>
    public enum LEF_StatusCode
    {
        /// <summary>
        /// La transacción se completó con exito
        /// </summary>
        SUCCESS = 200,
        /// <summary>
        /// La transacción no encontró datos
        /// </summary>
        NO_DATA_FOUND = 404,
        /// <summary>
        /// Hubo un error grave en el codigó que se ejecutó
        /// </summary>
        ERROR_CLASS = 500,
        /// <summary>
        /// Hubo un error con la conexión a Sql
        /// </summary>
        ERROR_SQL_CONTEXT = 1,
        /// <summary>
        /// La conexión a Sql se encuentra ocupada
        /// </summary>
        CONNECTION_BUZY = 2,
        /// <summary>
        /// Hay un error con la instrucción que Sql trató de ejecutar
        /// </summary>
        SQL_STATEMENT_WAS_WRONG = 3,
        /// <summary>
        /// 
        /// </summary>
        ERROR_ROLLBACK = 4,
        /// <summary>
        /// 
        /// </summary>
        SQL_EJECUTION_TIMEOUT = 5,
        /// <summary>
        /// 
        /// </summary>
        SQL_PK_EXEPTION = 6
    }

    /// <summary>
    /// Status Code Extension
    /// </summary>
    public static class SCE
    {
        public static int ToInt(this LEF_StatusCode _StatusCode) => (int)_StatusCode;
    }
}

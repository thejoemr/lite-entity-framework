﻿namespace LEF.Helpers
{
    /// <summary>
    /// Objeto de retorno generico
    /// </summary>
    /// <typeparam name="T">Tipo de dato esperado</typeparam>
    public class Transaction<T>
    {
        /// <summary>
        /// Resultado de la transacción
        /// </summary>
        public T Result { get; set; }

        /// <summary>
        /// Código de estado de la transacción
        /// </summary>
        public int StatusCode { get; set; } = LEF_StatusCode.SUCCESS.ToInt();

        /// <summary>
        /// Log del la transacción (NO OBLIGATORIO)
        /// </summary>
        public string Log { get; set; }
    }
}

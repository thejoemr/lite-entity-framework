namespace LEF.Helpers
{
    /// <summary>
    /// Metodos extensores de la clase Transaction
    /// </summary>
    public static class TransactionExtensions
    {
        /// <summary>
        /// Verifica que una transacción se cumpla de manera correcta
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Transaction"></param>
        /// <returns></returns>
        public static bool IsSuccess<T>(this Transaction<T> Transaction)
        {
            if (Transaction.StatusCode != LEF_StatusCode.SUCCESS.ToInt())
                return false;

            return true;
        }
        
        
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="Transaction"></param>
        /// <param name="OtherTransaction"></param>
        /// <returns></returns>
        public static Transaction<T> Transfer<T,U>(this Transaction<T> Transaction, Transaction<U> OtherTransaction)
        {
            Transaction.StatusCode = OtherTransaction.StatusCode;
            Transaction.Log = OtherTransaction.Log;

            return Transaction;
        }
    }
}

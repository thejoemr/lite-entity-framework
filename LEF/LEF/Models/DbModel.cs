﻿using LEF.Helpers;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace LEF.Models
{
    /// <summary>
    /// Distingue un modelo de datos de otro tipo de clases en tu proyecto
    /// </summary>
    public abstract class DbModel
    {
        /// <summary>
        /// Obtiene las propiedades <strong>no compuestas</strong> de un modelo
        /// </summary>
        /// <typeparam name="T">Model Type</typeparam>
        /// <param name="IncludeInheritance">Define si quieres que se incluyan propiedades heredadas o no (true/false)</param>
        /// <returns>
        /// Lista de propiedades del modelo
        /// </returns>
        public static List<PropertyInfo> GetProperties<T>(bool IncludeInheritance = false) where T : DbModel
        {
            var entity = Activator.CreateInstance(typeof(T));

            var propiedadesSinHerencia = entity.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            var propiedadesConHerencia = entity.GetType().GetProperties();

            var propiedades = IncludeInheritance ? propiedadesConHerencia : propiedadesSinHerencia;

            var output = new List<PropertyInfo>();

            foreach (PropertyInfo propiedad in propiedades)
            {
                Composition compositionAttribute = propiedad.GetCustomAttribute<Composition>();

                /*
                 * Las propiedades de composición no son parte del modelo de datos, son un añadido controlado por el desarrollador.
                 * Por tal motivo no se añaden a la lista
                 */
                if (compositionAttribute is null)
                    output.Add(propiedad);
            }

            return output;
        }

        /// <summary>
        /// Obtiene el nombre de la entidad Sql a la que hace referencia un modelo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string GetEntitylName<T>()
        {
            var modelName = string.Empty;

            var attributes = Attribute.GetCustomAttributes(typeof(T));
            foreach (Attribute attr in attributes)
                if (attr is Model modelAttr)
                    modelName = modelAttr.ModelName;

            if (modelName is null)
                throw new Exception("No se ha especificado el [Model] atributo en la clase correspondiente");

            return modelName;
        }

        /// <summary>
        /// Obtiene información del modelo para poder armar una instrucción Sql
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Model">Modelo de datos</param>
        /// <param name="Preferences">Preferencias de recolección de información</param>
        /// <param name="SqlPrefix">Prefijo para los parametros Sql</param>
        /// <returns></returns>
        public static DbRequestInfo<T> GetRequestInfo<T>(T Model, PropertyPreferences Preferences, string SqlPrefix = null) where T : DbModel
        {
            if (Model is null)
                throw new Exception("No puedes ingresar objetos nulos".ToUpper());

            if (Preferences is null)
                Preferences = new PropertyPreferences();

            var properties = Model.GetProperties(Preferences);

            var output = new DbRequestInfo<T>
            {
                Model = Model,
                PropertiesInfo = properties,
                SqlParameters = new List<SqlParameter>()
            };

            foreach (var propName in properties.ListProperty<PropertyInfo, string>("Name"))
            {
                var paramName = $"@{propName}{SqlPrefix}";

                var propValue = Model.GetType().GetProperty(propName).GetValue(Model);
                var paramVaue = propValue is null ? DBNull.Value : propValue;

                var parameter = new SqlParameter(paramName, paramVaue);

                output.SqlParameters.Add(parameter);
            }

            return output;
        }

        /// <summary>
        /// Convierte una lista de objetos en una DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="DataSource"></param>
        /// <returns></returns>
        public static DataTable ConvertToTable<T>(params T[] DataSource) where T : DbModel
        {
            try
            {
                if (DataSource is null || DataSource.Length == 0)
                    throw new Exception("La fuente de datos no puede ser nula".ToUpper());

                DataTable output = new DataTable();

                var Properties = GetProperties<T>();

                foreach (var property in Properties)
                    output.Columns.Add(new DataColumn(property.Name, property.PropertyType));

                foreach (var obj in DataSource)
                {
                    DataRow row = output.NewRow();

                    foreach (var prop in Properties)
                    {
                        var property = obj.GetType().GetProperty(prop.Name);
                        object value = prop.GetValue(obj);

                        row[prop.Name] = value;
                    }

                    output.Rows.Add(row);
                }

                return output;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToUpper());
            }
        }
    }
}

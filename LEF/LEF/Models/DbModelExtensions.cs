﻿using System.Collections.Generic;
using System.Reflection;

namespace LEF.Models
{
    /// <summary>
    /// Metodos extensores de un modelo de datos
    /// </summary>
    public static class DbModelExtensions
    {
        /// <summary>
        /// Obtiene las propiedades de un modelo especifico basandose en una configuración especial
        /// </summary>
        /// <typeparam name="T">Model Type</typeparam>
        /// <param name="Model">Modelo de datos</param>
        /// <param name="Settings">Configuracíón para la recuperación de datos</param>
        /// <returns></returns>
        public static List<PropertyInfo> GetProperties<T>(this T Model, PropertyPreferences Settings) where T : DbModel
        {
            var properties = DbModel.GetProperties<T>(Settings.IncludeInheritance);

            var output = new List<PropertyInfo>();

            foreach (PropertyInfo property in properties)
            {
                var IsPk = property.GetCustomAttribute<PrimaryKey>() != null;
                var HasNullValue = property.GetValue(Model) is null;

                if ((IsPk && !Settings.IncludePk) || (HasNullValue && !Settings.IncludeNullValues))
                    continue;

                output.Add(property);
            }

            return output;
        }
    }
}

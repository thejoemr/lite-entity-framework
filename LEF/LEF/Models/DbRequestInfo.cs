﻿using LEF.Helpers;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace LEF.Models
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DbRequestInfo<T> where T : DbModel
    {
        /// <summary>
        /// Modelo de referencia
        /// </summary>
        public T Model { get; set; }

        /// <summary>
        /// Propiedades que se tomaron del modelo para usar en la instrucción Sql
        /// </summary>
        public List<PropertyInfo> PropertiesInfo { get; set; }

        /// <summary>
        /// Parametros Sql, tomados a partir de los valores de las propiedades usadas del modelo
        /// </summary>
        public List<SqlParameter> SqlParameters { get; set; }

        /// <summary>
        /// Define la estructura de una instrucción <strong>UPDATE</strong> 
        /// <para>Ejemplo: <strong>UPDATE [entityName] SET [columna...] = [parametro..]</strong></para>
        /// </summary>
        /// <returns></returns>
        public string GetUpdateStatement()
        {
            var propertyNames = PropertiesInfo.ListProperty<PropertyInfo, string>("Name");

            /*
             * Validamos que el numero de propiedades usadas por el modelo sea el mismo que de
             * parametros de Sql, de ser el mismo, validamos que propiedad y parametro hagan referencia al mismo dato
             */
            if(PropertiesInfo.Count != SqlParameters.Count)
                throw new Exception("El numero de propiedades y parametros del modelo debe ser el mismo".ToUpper());

            foreach (var propName in propertyNames)
                if (!SqlParameters.Exists(x => x.ParameterName.Contains($"@{propName}")))
                    throw new Exception("Las propiedades y parametros del modelo no hacen referencia a los mismos datos".ToUpper());

            var setValues = new List<string>();
            propertyNames.ForEach(propName => setValues.Add($"{propName} = {SqlParameters.Find(x => x.ParameterName.Contains($"@{propName}")).ParameterName}"));

            return $"UPDATE {DbModel.GetEntitylName<T>()} SET {string.Join(", ", setValues)}";
        }

        /// <summary>
        /// Define la estructura de una instrucción <strong>INSERT</strong> 
        /// <para>Ejemplo: <strong>INSERT INTO [entityName] ([columna...]) VALUES ([parametro..])</strong></para>
        /// </summary>
        /// <returns></returns>
        public string GetInsertStatement()
        {
            var propertyNames = PropertiesInfo.ListProperty<PropertyInfo, string>("Name");

            /*
             * Validamos que el numero de propiedades usadas por el modelo sea el mismo que de
             * parametros de Sql, de ser el mismo, validamos que propiedad y parametro hagan referencia al mismo dato
             */
            if (PropertiesInfo.Count != SqlParameters.Count)
                throw new Exception("El numero de propiedades y parametros del modelo debe ser el mismo".ToUpper());

            foreach (var propName in propertyNames)
                if (!SqlParameters.Exists(x => x.ParameterName.Contains($"@{propName}")))
                    throw new Exception("Las propiedades y parametros del modelo no hacen referencia a los mismos datos".ToUpper());

            var parameterNames = SqlParameters.ListProperty<SqlParameter, string>("ParameterName");

            return $"INSERT INTO {DbModel.GetEntitylName<T>()} ({string.Join(", ", propertyNames)}) VALUES ({string.Join(", ", parameterNames)})";
        }

        /// <summary>
        /// Define la estructura de una instrucción <strong>SELECT</strong> 
        /// <para>Ejemplo: <strong>SELECT [columna..] FROM [entityName]</strong></para>
        /// </summary>
        /// <returns></returns>
        public string GetSelectStatement()
        {
            var propertyNames = PropertiesInfo.ListProperty<PropertyInfo, string>("Name");

            return $"SELECT {string.Join(", ", propertyNames)} FROM {DbModel.GetEntitylName<T>()}";
        }
    }
}

﻿using LEF.SQL;

namespace LEF.Models
{
    /// <summary>
    /// Define comportamiento especial en un modelo de datos
    /// </summary>
    public interface IDbModel<T> where T : DbModel
    {
        /// <summary>
        /// Usa este metodo para armar los datos faltantes en tu modelo de datos, como por ejemplo:
        /// <para>Propiedades de composicion</para>
        /// </summary>
        /// <returns></returns>
        T Assemble(DbContext context);
    }
}

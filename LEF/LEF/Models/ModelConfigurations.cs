﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LEF.Models
{
    /// <summary>
    /// Representa distintos tipos de entidades de datos
    /// </summary>
    public enum ModelType
    {
        /// <summary>
        /// Tabla
        /// </summary>
        TABLE,
        /// <summary>
        /// Vista
        /// </summary>
        VIEW,
        /// <summary>
        /// Tipo de tabla definido por el usuario
        /// </summary>
        TYPE
    }

    /// <summary>
    /// Especifica las propiedades de modelo de datos
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class Model : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ModelName">Nombre del modelo</param>
        /// <param name="ModelType">Tipo de modelo</param>
        public Model(string ModelName, ModelType ModelType)
        {
            this.ModelName = ModelName;
            this.ModelType = ModelType;
        }

        /// <summary>
        /// Nombre del modelo
        /// </summary>
        public string ModelName { get; }

        /// <summary>
        /// Tipo de modelo
        /// </summary>
        public ModelType ModelType { get; }
    }

    /// <summary>
    /// Define una llave primaria
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class PrimaryKey : Attribute { }

    /// <summary>
    /// Define una llave foranea
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ForeignKey : Attribute
    {
        /// <summary>
        /// Crea una llave foranea
        /// </summary>
        /// <param name="Reference">Modelo de referencia</param>
        /// <param name="PropertyName">Nombre de la propiedad de referencia</param>
        public ForeignKey(Type Reference, string PropertyName)
        {
            this.Reference = Reference;
            this.PropertyName = PropertyName;
        }

        /// <summary>
        /// Modelo de referencia
        /// </summary>
        public Type Reference { get; }

        /// <summary>
        /// Nombre de la propiedad de referencia
        /// </summary>
        public string PropertyName { get; }
    }

    /// <summary>
    /// Especificar que la propiedad NO PERTENECEN al modelo de datos
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class Composition : Attribute { }
}

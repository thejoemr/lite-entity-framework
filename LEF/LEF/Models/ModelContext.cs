﻿using LEF.Helpers;
using LEF.SQL;
using System;
using System.Collections.Generic;
using System.Text;

namespace LEF.Models
{
    /// <summary>
    /// Define propiedades generales de un contexto de datos
    /// </summary>
    /// <typeparam name="T">Type of model that you will use</typeparam>
    public abstract class ModelContext<T> where T : DbModel
    {
        /// <summary>
        /// Contexto de trabajo de la base de datos
        /// </summary>
        public DbContext DbContext { get; }

        /// <summary>
        /// Nombre del modelo
        /// </summary>
        public string ModelName { get; }

        /// <summary>
        /// Preferencias para la recopilación de información
        /// </summary>
        public PropertyPreferences DefaultPreferences { get; set; } = new PropertyPreferences();

        /// <summary>
        /// Crea un nuevo contexto de datos
        /// </summary>
        /// <param name="Db">DB context</param>
        public ModelContext(DbContext Db)
        {
            this.ModelName = DbModel.GetEntitylName<T>();
            this.DbContext = Db;
        }
    }
}

﻿namespace LEF.Models
{
    /// <summary>
    /// Preferencias para la recopilación de información de un modelo
    /// </summary>
    public class PropertyPreferences
    {
        /// <summary>
        /// Define si se incluirán las llaves primarias en las transacciónes de datos
        /// </summary>
        public bool IncludePk { get; set; }

        /// <summary>
        /// Define si se incluirán las propiedades heredadas en las transacciónes de datos
        /// </summary>
        public bool IncludeInheritance { get; set; }

        /// <summary>
        /// Define si se incluirán valores nulos en las transacciónes de datos
        /// </summary>
        public bool IncludeNullValues { get; set; }

    }
}

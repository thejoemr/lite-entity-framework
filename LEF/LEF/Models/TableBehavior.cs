﻿using LEF.CRUD;
using LEF.SQL;
using System;
using System.Collections.Generic;
using System.Text;

namespace LEF.Models
{
    /// <summary>
    /// Definición de compartimiento <strong>CRUD</strong> de una entidad de datos de tipo <strong>Tabla</strong>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class TableBehavior<T> : ModelContext<T> where T : DbModel
    {
        /// <summary>
        /// Para insertar nuevos registros
        /// </summary>
        public InsertAction<T> Create { get; }

        /// <summary>
        /// Para eliminar registros
        /// </summary>
        public DeleteAction<T> Delete { get; }

        /// <summary>
        /// Para actualizar registros
        /// </summary>
        public UpdateAction<T> Update { get; }

        /// <summary>
        /// Para obtener información
        /// </summary>
        public SelectAction<T> Read { get; }

        /// <summary>
        /// Crea el entorno de interacción con el modelo de datos
        /// </summary>
        /// <param name="DbContext"></param>
        public TableBehavior(DbContext DbContext) : base(DbContext)
        {
            Create = new InsertAction<T>(this);
            Delete = new DeleteAction<T>(this);
            Update = new UpdateAction<T>(this);
            Read = new SelectAction<T>(this);
        }
    }
}

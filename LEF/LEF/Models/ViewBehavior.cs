﻿using LEF.CRUD;
using LEF.SQL;
using System;
using System.Collections.Generic;
using System.Text;

namespace LEF.Models
{
    /// <summary>
    /// Definición de compartimiento <strong>SOLO LECTURA</strong> de una entidad de datos de tipo:
    /// <para><strong>Tabla</strong></para>
    /// <para><strong>Vista</strong></para>
    /// <para><strong>Tipo de tabla definida por el usuario</strong></para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ViewBehavior<T> : ModelContext<T> where T : DbModel
    {
        /// <summary>
        /// Para obtener información
        /// </summary>
        public SelectAction<T> Read { get; }

        /// <summary>
        /// Crea el entorno de interacción con el modelo de datos
        /// </summary>
        /// <param name="Transactions"></param>
        public ViewBehavior(DbContext Transactions) : base(Transactions)
        {
            Read = new SelectAction<T>(this);
        }
    }
}

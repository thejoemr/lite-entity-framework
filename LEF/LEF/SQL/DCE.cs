﻿using LEF.Helpers;
using Microsoft.Data.SqlClient;
using System;
using System.Data;

namespace LEF
{
    /// <summary>
    /// Db Connection Extensions
    /// </summary>
    public static class DCE
    {
        /// <summary>
        /// Verifica la disponibilidad de una conexión Sql
        /// </summary>
        /// <param name="Connection">Conexión Sql</param>
        /// <returns>
        /// <para>IsDisponible: Si conexión está o nó disponible (true/false)</para>
        /// <para>Log: Log de estado de la conexón</para>
        /// <para>StatusCode: Código de estado de la operación</para>
        /// </returns>
        public static (bool IsDisponible, string Log, LEF_StatusCode StatusCode) IsAvailable(this SqlConnection Connection)
        {
            try
            {
                if (Connection.State == ConnectionState.Executing)
                    return (false, "LA CONEXIÓN SE ENCUENTRA EJECUTANDO OTRA TRANSACCIÓN", LEF_StatusCode.CONNECTION_BUZY);
                if (Connection.State == ConnectionState.Fetching)
                    return (false, "LA CONEXÍON SE ENCUENTRA RECUPERANDO DATOS", LEF_StatusCode.CONNECTION_BUZY);
                if (Connection.State == ConnectionState.Connecting)
                    return (false, "LA CONEXIÓN ESTÁ TRATANDO DE CONECTARSE AL MOTOR DE BASE DE DATOS", LEF_StatusCode.CONNECTION_BUZY);
                else
                {
                    Connection.Close();
                    Connection.Open();
                    Connection.Close();

                    return (true, "LA CONEXIÓN ESTÁ DISPONIBLE", LEF_StatusCode.SUCCESS);
                }
            }
            catch (Exception ex)
            {
                return (false, ex.Message.ToUpper(), LEF_StatusCode.ERROR_SQL_CONTEXT);
            }
        }
    }
}

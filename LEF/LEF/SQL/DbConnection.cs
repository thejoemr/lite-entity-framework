﻿using Microsoft.Data.SqlClient;
using System.Data;

namespace LEF.SQL
{
    /// <summary>
    /// Se encarga de controlar y almacenar una conexión Sql
    /// </summary>
    public class DbConnection
    {
        /// <summary>
        /// Conexión al motor de Sql
        /// </summary>
        public SqlConnection Connection { get; }

        /// <summary>
        /// Obtiene el status de la conexión
        /// </summary>
        /// <returns></returns>
        public ConnectionState ConnectionState => Connection.State;

        /// <summary>
        /// Cadena de conexión
        /// </summary>
        /// <returns></returns>
        public string ConnectionString { get; }

        /// <summary>
        /// Crea una nueva conexión de datos
        /// </summary>
        /// <param name="ConnectionString">Cadena de conexión</param>
        public DbConnection(string ConnectionString)
        {
            this.ConnectionString = ConnectionString;
            Connection = new SqlConnection(this.ConnectionString);
        }
    }
}

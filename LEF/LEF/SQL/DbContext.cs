﻿using LEF.Helpers;
using System.Data;

namespace LEF.SQL
{
    /// <summary>
    /// Contexto de base de datos:
    /// <para>Conexión a la base de datos</para>
    /// <para>Manejador de transacciónes</para> 
    /// </summary>
    public class DbContext
    {
        /// <summary>
        /// Conexión Sql
        /// </summary>
        public DbConnection DbConnection { get; }

        /// <summary>
        /// Crea un nuevo contexto
        /// </summary>
        /// <param name="DbConnection">Conexión a la base de datos</param>
        public DbContext(DbConnection DbConnection)
        {
            this.DbConnection = DbConnection;
        }

        /// <summary>
        /// Executa una instruccion Sql <strong>sin consultas de datos</strong>
        /// </summary>
        /// <param name="Request">Transacción de datos</param>
        /// <returns>
        /// <strong>True</strong> si el número de filas es mayor a cero, <strong>False</strong> si es igual o menor a 0
        /// </returns>
        public Transaction<bool> ExecBoolNonQuery(DbRequest Request) => DbExecute.NonQuery<bool>(Request, DbConnection);

        /// <summary>
        /// Executa una instruccion Sql <strong>sin consultas de datos</strong>
        /// </summary>
        /// <param name="Request">Transacción de datos</param>
        /// <returns>
        /// Número de filas afectadas
        /// </returns>
        public Transaction<int> ExecIntNonQuery(DbRequest Request) => DbExecute.NonQuery<int>(Request, DbConnection);

        /// <summary>
        /// Ejecuta una instricción Sql que contenga consultas de datos <strong>(RETORNA 1 O MÁS TABLAS)</strong>
        /// </summary>
        /// <param name="Request">Transacción de datos/param>
        /// <returns>
        /// Tabla solicitada
        /// </returns>
        public Transaction<DataTable> ExecWithTable(DbRequest Request) => DbExecute.Query<DataTable>(Request, DbConnection);

        /// <summary>
        /// Ejecuta una instrucción Sql <strong>con consulta(s) de datos</strong>
        /// </summary>
        /// <param name="Request">Transacción de datos</param>
        /// <returns>
        /// Conjunto de tablas solicitadas
        /// </returns>
        public Transaction<DataSet> ExecWithDataSet(DbRequest Request) => DbExecute.Query<DataSet>(Request, DbConnection);
    }
}

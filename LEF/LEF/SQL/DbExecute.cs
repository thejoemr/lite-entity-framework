﻿using LEF.Helpers;
using Microsoft.Data.SqlClient;
using System;
using System.Data;
using System.Text;

namespace LEF.SQL
{
    /// <summary>
    /// Comportamiento generico de ejecución de transacciónes Sql
    /// </summary>
    public static class DbExecute
    {
        /// <summary>
        /// Executa una instruccion Sql <strong>sin consultas de datos</strong>
        /// </summary>
        /// <typeparam name="T">Output Type</typeparam>
        /// <param name="Request">Transacción de datos</param>
        /// <param name="DbConnection">Conexión a base datos</param>
        /// <returns>
        /// <para><strong>Si (T = Int):</strong> Número de filas afectadas</para>
        /// <para><strong>Si (T = Bool):</strong> True si el número de filas es mayor a cero, False si es igual o menor a 0</para>
        /// </returns>
        public static Transaction<T> NonQuery<T>(DbRequest Request, DbConnection DbConnection)
        {
            var Connection = DbConnection.Connection;

            if (Connection.ConnectionString is null || Connection.ConnectionString.Equals(string.Empty))
                Connection.ConnectionString = DbConnection.ConnectionString;

            var (IsDisponible, Log, StatusCode) = Connection.IsAvailable();

            //Si la conexión no está disponible...
            if (!IsDisponible)
                return new Transaction<T>()
                {
                    StatusCode = StatusCode.ToInt(),
                    Log = Log
                };

            using (Connection)
            {
                Connection.Open();

                using SqlCommand command = Connection.CreateCommand();

                if (Request.IsSP)
                    command.CommandType = CommandType.StoredProcedure;
                else
                    command.CommandType = CommandType.Text;

                //Si hay parametros que añadir
                if (Request.Parameters != null)
                    command.Parameters.AddRange(Request.Parameters.ToArray());

                command.CommandTimeout = 5000;

                using SqlTransaction transaction = Connection.BeginTransaction("LEF_Transaction");
                command.Connection = Connection;
                command.Transaction = transaction;
                command.CommandText = Request.Statement;

                try
                {
                    int rows = command.ExecuteNonQuery();
                    transaction.Commit();

                    var output = new Transaction<T>() { Log = $"{rows} FILAS MODIFICADAS" };

                    if (rows == 0)
                    {
                        output.Result = typeof(T) == typeof(bool) ? (T)Convert.ChangeType(false, typeof(T)) : (T)Convert.ChangeType(0, typeof(T));
                        output.StatusCode = LEF_StatusCode.NO_DATA_FOUND.ToInt();
                    }
                    else
                        output.Result = typeof(T) == typeof(bool) ? (T)Convert.ChangeType(true, typeof(T)) : (T)Convert.ChangeType(rows, typeof(T));

                    return output;
                }
                catch (Exception ex)
                {
                    try
                    {
                        //Intentamos dar roll back a la consulta
                        transaction.Rollback();

                        if (ex is SqlException sqlEx)
                        {
                            //Error de primary key, se intentó insertar una PK existente
                            if (sqlEx.Number == 2627)
                                return new Transaction<T>()
                                {
                                    StatusCode = LEF_StatusCode.SQL_PK_EXEPTION.ToInt(),
                                    Log = $"ERROR EN LA CONSULTA (PK EXCEPTION): {ex.Message.ToUpper()}"
                                };

                            //Interblock exception, debemos volver a intentar
                            if (sqlEx.Number == 1025)
                                return NonQuery<T>(Request, DbConnection);
                        }


                        return new Transaction<T>()
                        {
                            StatusCode = LEF_StatusCode.SQL_STATEMENT_WAS_WRONG.ToInt(),
                            Log = $"ERROR EN LA CONSULTA: {ex.Message.ToUpper()}"
                        };
                    }
                    catch (Exception ex2)
                    {
                        return new Transaction<T>()
                        {
                            StatusCode = LEF_StatusCode.ERROR_ROLLBACK.ToInt(),
                            Log = $"ERROR (ROLLBACK/RETRY EXCEPTION): {ex2.Message.ToUpper()}"
                        };
                    }
                }
            }
        }

        /// <summary>
        /// Ejecuta una instrucción Sql <strong>con consulta(s) de datos</strong>
        /// </summary>
        /// <typeparam name="T">Output Type</typeparam>
        /// <param name="Request">Transacción de datos</param>
        /// <param name="DbConnection">Conexión a base datos</param>
        /// <returns>
        /// <para><strong>Si (T = DataTable):</strong> Tabla solicitada</para>
        /// <para><strong>Si (T = DataSet):</strong> Conjunto de tablas solicitadas</para>
        /// </returns>
        public static Transaction<T> Query<T>(DbRequest Request, DbConnection DbConnection)
        {
            var Connection = DbConnection.Connection;

            if (Connection.ConnectionString is null || Connection.ConnectionString.Equals(string.Empty))
                Connection.ConnectionString = DbConnection.ConnectionString;

            var (IsDisponible, Log, StatusCode) = Connection.IsAvailable();

            //Si la conexión no está disponible...
            if (!IsDisponible)
                return new Transaction<T>()
                {
                    StatusCode = StatusCode.ToInt(),
                    Log = Log
                };

            using (Connection)
            {
                Connection.Open();

                using SqlCommand command = Connection.CreateCommand();

                if (Request.IsSP)
                    command.CommandType = CommandType.StoredProcedure;
                else
                    command.CommandType = CommandType.Text;

                //Si hay parametros que añadir
                if (Request.Parameters != null)
                    command.Parameters.AddRange(Request.Parameters.ToArray());

                command.CommandTimeout = 5000;

                command.Connection = Connection;

                try
                {
                    command.CommandText = Request.Statement;

                    var output = new Transaction<T>();

                    if (typeof(T) == typeof(DataTable))//Si el retorno es un DataTable...
                    {
                        SqlDataReader sqlDataReader = command.ExecuteReader();
                        using (sqlDataReader)
                        {
                            var tbl = new DataTable();

                            tbl.Load(sqlDataReader);

                            output.Result = (T)Convert.ChangeType(tbl, typeof(T));

                            if (tbl.Rows.Count == 0)
                            {
                                output.Log = "NO ENCONTRAMOS DATOS";
                                output.StatusCode = LEF_StatusCode.NO_DATA_FOUND.ToInt();
                            }
                        };   
                    }
                    else//Si el retorno es un DataSet
                    {
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        using (sqlDataAdapter)
                        {
                            var ds = new DataSet();

                            sqlDataAdapter.Fill(ds);

                            output.Result = (T)Convert.ChangeType(ds, typeof(T));

                            if (ds.Tables.Count == 0)
                            {
                                output.Log = "NO ENCONTRAMOS DATOS";
                                output.StatusCode = LEF_StatusCode.NO_DATA_FOUND.ToInt();
                            }
                        };
                    }

                    return output;
                }
                catch (Exception ex)
                {
                    if (ex is SqlException sqlEx)
                    {
                        //Error de primary key, se intentó insertar una PK existente
                        if (sqlEx.Number == 2627)
                            return new Transaction<T>()
                            {
                                StatusCode = LEF_StatusCode.SQL_PK_EXEPTION.ToInt(),
                                Log = $"ERROR EN LA CONSULTA (PK EXCEPTION): {ex.Message.ToUpper()}"
                            };

                        //Interblock exception, debemos volver a intentar
                        if (sqlEx.Number == 1025)
                            return Query<T>(Request, DbConnection);
                    }

                    return new Transaction<T>()
                    {
                        StatusCode = LEF_StatusCode.SQL_STATEMENT_WAS_WRONG.ToInt(),
                        Log = $"ERROR EN LA CONSULTA: {ex.Message.ToUpper()}"
                    };
                }
            }
        }
    }
}

﻿using Microsoft.Data.SqlClient;
using System.Collections.Generic;

namespace LEF.SQL
{
    /// <summary>
    /// Instrucción Sql
    /// </summary>
    public class DbRequest
    {
        /// <summary>
        /// Instrucción Sql: <strong>Consulta o nombre del procedimiento almacenado</strong>
        /// </summary>
        public string Statement { get; set; }

        /// <summary>
        /// Especifica si la instrucción Sql es un procedimiento almacenado o no (true/false)
        /// </summary>
        public bool IsSP { get; set; }

        /// <summary>
        /// Conjunto de parametros que utiliza la instrucción Sql (NO OBLIGATORIO PERO RECOMENDADO)
        /// </summary>
        public List<SqlParameter> Parameters { get; set; }

        /// <summary>
        /// Crea una nueva solicitud de ejecución
        /// </summary>
        /// <param name="Statement"></param>
        public DbRequest(string Statement)
        {
            this.Statement = Statement;
        }

        /// <summary>
        /// Crea una nueva solicitud de ejecución
        /// </summary>
        public DbRequest()
        {

        }
    }
}
